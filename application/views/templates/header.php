<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://bootswatch.com/3/flatly/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/style.css">
 
    <script src="https://cdn.ckeditor.com/ckeditor5/37.1.0/classic/ckeditor.js" defer></script>
    <title>Ecommerce</title>
</head>
<body>

    <nav class="navbar navbar-inverse hue-blue">
        <div class="container-fluid">
          <div class="navbar-header">
           <i class=""><a class="navbar-brand header-logo" href="">
            <!-- <img class="icon-home bi bi-bag" 
            src="../../../assets/images/icons/shoplogo.png" 
            alt="Logo" /> -->
            <svg xmlns="http://www.w3.org/2000/svg" width="17" height="29" fill="currentColor" class=" bi bi-bag"
            viewBox="0 0 17 29">
              <path
                d="M8 1a2.5 2.5 0 0 1 2.5 2.5V4h-5v-.5A2.5 2.5 0 0 1 8 1zm3.5 3v-.5a3.5 3.5 0 1 0-7 0V4H1v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4h-3.5zM2 5h12v9a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V5z" />
            </svg>
          </a></i>
          </div>
    
        <ul class="nav navbar-nav">
              <li><a href="<?php echo base_url(); ?>">Home</a></li>
              <li><a href="<?php echo base_url(); ?>about">About</a></li>
              <li><a href="">Products</a></li>  
              <li><a href="">My Orders</a></li>  
        </ul>
        <ul class="nav navbar-nav navbar-right">

        <li><a href="">Register</a></li>
        <li><a href="">Login</a></li>
      
  
          <!-- <li><a href="">Create Post</a></li> -->
          <li><a href="">Admin Dashboard</a></li> 
          <li><a href="">Log Out</a></li>
       
        </ul>
      
  
  
      </div>
    </div>
  </nav>
  
  
  
      <div class="container">